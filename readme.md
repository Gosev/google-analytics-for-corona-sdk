Google Analytics module for Corona SDK
======================================

To enable Google Analytics create a new "Mobile App" property on Google Analytics
and get a Tracking ID to start testing with. Make sure Internet is enabled in 
build settings for Android.


In main.lua
-----------

    local ga = require("GoogleAnalytics.ga")
    
    ga.init({ -- Only initialize once, not in every file
        isLive = false, -- REQUIRED
        testTrackingID = "UA-123abc-1", -- REQUIRED Tracking ID from Google
        debug = true, -- Recomended when starting
    })

Full list of available init params
----------------------------------

* isLive = false 
    * REQUIRED The module assumes you will have 2 tracking IDs, one for testing and another for production. isLive = true will use production tracking ID. Simulator always uses testing ID.

* testTrackingID = "UA-123abc-1"
    * REQUIRED Tracking ID from GA, used for testing. If this ID is wrong there is no error reported.

* productionTrackingID = "UA-123abc-2",
    * Tracking ID from GA used when app is live. Simulator always uses testing ID. Required once isLive = true

* debug = true
    * See debug print messages. Recomend to set to true to start with. Default value: false  

* appName = "My great app"
    * Application name. Default value: system.getInfo("appName")
        * http://docs.coronalabs.com/api/library/system/getInfo.html#appname
        * https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters#an

* appID = "com.myCompany.greatApp",
    * Application ID. Default value: nil
        * https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters#aid

* appVersion = "1.2",
    * Application version. Default value: system.getInfo("appVersionString")
        * http://docs.coronalabs.com/api/library/system/getInfo.html#appversionstring
        * https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters#av

* protocolVersion = 1
    * Protocol Version. Default value: 1 
        * https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters#v

* clientID = "123abc"
    * Unique client (device) ID. Recommend keeping this on default value: system.getInfo("deviceID")
        * http://docs.coronalabs.com/api/library/system/getInfo.html#deviceid
        * https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters#cid

* userID = "123abc"
    * Unique user ID. Default value: nil
        * https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters#uid
        * https://support.google.com/analytics/answer/3123662?hl=en

* additionalDefaultParams = {cd1 = "Paid user"},
    * A table of additional default params that are sent with every request. Perhaps a custom dimension. Default: nil

* sessionParams = {cd2 = system.getInfo("targetAppStore")},
    * A table of additional session level params. For example a session level custom dimension. Default: nil


Storyboard scene tracking
-------------------------
Optional. Enable automatic storyboard scene tracking. Sorry no Composer support at the moment. Call this once after ga.init()

    ga.enableStoryBoardScenes() 


Tracking events
---------------

IMPORTANT: For complete description of these function see the function comments in the ga.lua module. Examples here maybe missing available function parameters.

In any file you want to send events from.

    local ga = require("GoogleAnalytics.ga")

Track when user enters a scene. Not required if you use enableStoryBoardScenes().

    ga.enterScene(sceneName)

To track all general events. For example user turns off sound.

    ga.event("Options", "Sound", "Off") 

To track a social event.

    ga.social("Facebook", "Homepage", "Like") 

To track a purchase event.

    ga.purchase("someTransactionID", "Red car", "Cars", "com.myCompany.products.redcar", "1.99", "USD") 

To track a timed event

    ga.time(176, "Network", "Ping time")

Track errors

    ga.error("Nothing works anymore", true) -- Second parameter = isFatal? 

In addition these things are tracked automatically:
Session starting, Session ending, Screen resolution, User language

SEE THESE FUNCTION IN ga.lua FOR PROPER DESCRIPTION OF USAGE

User Agents
-----------
Google uses the network.request user agent to determine the device type.
However iOS devices do not send a usable user agent with network.requests.
Unlike network.request, native.newWebView uses a proper user agent. So with some
javascript in a local html file we use the webview to retrieve the user agent
and override the user agent when we send payloads to Google. This takes a few
moments and in the meanwhile some events may have happened. These events are not
sent until we know the user agent and are then sent as a latent event:
https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters#qt

Offline support
---------------
The module has limited offline support. Events that can not be sent due to missing
network or some other errors are saved. When there is a successful payload delivery 
the offline events are sent one after the other. Offline events are not saved to 
the filesystem so if the user or device kills the process (not suspended)
the events are lost.

Useful information
------------------

Basics:
https://developers.google.com/analytics/devguides/collection/protocol/v1/reference

Dev guide:
https://developers.google.com/analytics/devguides/collection/protocol/v1/devguide#apptracking

With any event you can mix and add additional parameters: 
https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters

You can add custom dimensions and metrics:
https://support.google.com/analytics/answer/2709829?hl=en

Limits and quotas:
https://developers.google.com/analytics/devguides/collection/ios/limits-quotas


Disclaimer: I'm not a Google Analytics expert, if something can be improved let me know!





The MIT License (MIT)
---------------------

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.